import numpy
from keras.datasets import imdb
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM, Convolution1D, Flatten, Dropout
from keras.layers.embeddings import Embedding
from keras.preprocessing import sequence
from keras.callbacks import TensorBoard
from keras.optimizers import SGD, Adagrad, Adadelta, RMSprop, Adam
#from plot import plot_graph
import matplotlib.pyplot as plt

# Using keras to load the dataset with the top_words
top_words = 10000
(X_train, y_train), (X_test, y_test) = imdb.load_data(num_words=top_words)

# Pad the sequences to the same length
max_review_length = 1600
X_train = sequence.pad_sequences(X_train, maxlen=max_review_length)
X_test = sequence.pad_sequences(X_test, maxlen=max_review_length)

# Using embedding from Keras
embedding_vecor_length = 300

def cnn_model():
    model = Sequential()
    model.add(Embedding(top_words, embedding_vecor_length, input_length=max_review_length))
    # Convolutional model (3x conv, flatten, 2x dense)
    model.add(Convolution1D(64, 3, padding='same'))
    model.add(Convolution1D(32, 3, padding='same'))
    model.add(Convolution1D(16, 3, padding='same'))
    model.add(Flatten())
    model.add(Dropout(0.2))
    model.add(Dense(180,activation='sigmoid'))
    model.add(Dropout(0.2))
    model.add(Dense(1,activation='sigmoid'))
    return (model)

learning_rate = 0.001
rmsprop = RMSprop(lr=learning_rate, decay=0.0)
sgd = SGD(lr=learning_rate, momentum=0.0, decay=0.0, nesterov=False) # set to default except lr
sgdm = SGD(lr=learning_rate, momentum=0.9, decay=0.0, nesterov=False) # set to default except lr
adam = Adam(lr=learning_rate, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
adagrad = Adagrad(lr=learning_rate, epsilon=1e-08, decay=0.0)

rms_model = cnn_model()
sgd_model = cnn_model()
sgdm_model = cnn_model()
adam_model = cnn_model()
adagrad_model = cnn_model()

rms_model.compile(loss='binary_crossentropy',
              optimizer=rmsprop,
              metrics=['accuracy'])
print(rms_model.summary())
sgd_model.compile(loss='binary_crossentropy',
              optimizer=sgd,
              metrics=['accuracy'])
print(sgd_model.summary())
sgdm_model.compile(loss='binary_crossentropy',
              optimizer=sgdm,
              metrics=['accuracy'])
print(sgdm_model.summary())
adam_model.compile(loss='binary_crossentropy',
              optimizer=adam,
              metrics=['accuracy'])
print(adam_model.summary())
adagrad_model.compile(loss='binary_crossentropy',
              optimizer=adagrad,
              metrics=['accuracy'])
print(adagrad_model.summary())

# Log to tensorboard
#tensorBoardCallback = TensorBoard(log_dir='./logs', write_graph=True)
epochs=10
#model.fit(X_train, y_train, epochs=3, callbacks=[tensorBoardCallback], batch_size=64)
rms_history = rms_model.fit(X_train, y_train, epochs=epochs, batch_size=64,validation_data=(X_test, y_test))
sgd_history = sgd_model.fit(X_train, y_train,validation_split=0.01, epochs=epochs, batch_size=64,validation_data=(X_test, y_test))
sgdm_history = sgdm_model.fit(X_train, y_train,validation_split=0.01, epochs=epochs, batch_size=64,validation_data=(X_test, y_test))
adam_history = adam_model.fit(X_train, y_train,validation_split=0.01, epochs=epochs, batch_size=64,validation_data=(X_test, y_test))
adagrad_history = adagrad_model.fit(X_train, y_train,validation_split=0.01, epochs=epochs, batch_size=64,validation_data=(X_test, y_test))

# Evaluation on the test set
rms_scores = rms_model.evaluate(X_test, y_test, verbose=0)
sgd_scores = sgd_model.evaluate(X_test, y_test, verbose=0)
sgdm_scores = sgdm_model.evaluate(X_test, y_test, verbose=0)
adam_scores = adam_model.evaluate(X_test, y_test, verbose=0)
adagrad_scores = adagrad_model.evaluate(X_test, y_test, verbose=0)
print("RMSProps Accuracy: %.2f%%" % (rms_scores[1]*100))
print("RMSProps Loss: %.2f%%" % (rms_scores[0]*100))
print("SGD Accuracy: %.2f%%" % (sgd_scores[1]*100))
print("SGD Loss: %.2f%%" % (sgd_scores[0]*100))
print("SGDM Accuracy: %.2f%%" % (sgdm_scores[1]*100))
print("SGDM Loss: %.2f%%" % (sgdm_scores[0]*100))
print("ADAM Accuracy: %.2f%%" % (adam_scores[1]*100))
print("ADAM Loss: %.2f%%" % (adam_scores[0]*100))
print("ADAGRAD Accuracy: %.2f%%" % (adagrad_scores[1]*100))
print("ADAGRAD Loss: %.2f%%" % (adagrad_scores[0]*100))

# fig = plt.figure(figsize=(12,8))
plt.plot(range(epochs),rms_history.history['val_acc'],label='RMSprop')
# plt.plot(range(epochs),rms_history.history['acc'],label='SGD')
plt.plot(range(epochs),sgd_history.history['val_acc'],label='SGD')
plt.plot(range(epochs),sgdm_history.history['val_acc'],label='SGDM')
plt.plot(range(epochs),adam_history.history['val_acc'],label='Adam')
plt.plot(range(epochs),adagrad_history.history['val_acc'],label='Adagrad')


plt.legend(loc=0)
plt.xlabel('epochs')
plt.xlim([0,epochs])
plt.ylabel('accuracy om validation set')
# plt.grid(True)
plt.title("Comparing Model Accuracy")
plt.show()
plt.savefig('compare-accuracy_epoch_10_lr_0.001_drop_0.2.png')

plt.close()
plt.cla()
plt.clf()

plt.plot(range(epochs),rms_history.history['val_loss'],label='RMSprop')
plt.plot(range(epochs),sgd_history.history['val_loss'],label='SGD')
plt.plot(range(epochs),sgdm_history.history['val_loss'],label='SGDM')
plt.plot(range(epochs),adam_history.history['val_loss'],label='Adam')
plt.plot(range(epochs),adagrad_history.history['val_loss'],label='Adagrad')
# plt.plot(range(epochs),rms_history.history['loss'],label='SGD')
plt.legend(loc=0)
plt.xlabel('epochs')
plt.xlim([0,epochs])
plt.ylabel('loss om validation set')
# plt.grid(True)
plt.title("Comparing Model Loss")
plt.show()
plt.savefig('compare-loss_epoch_10_lr_0.001_drop_0.2.png')
plt.close()

# # list all data in history
# print(rms_history.history.keys())
# # summarize history for accuracy
# plt.plot(rms_history.history['acc'])
# plt.plot(rms_history.history['val_acc'])
# plt.title('model accuracy')
# plt.ylabel('accuracy')
# plt.xlabel('epoch')
# plt.legend(['train', 'test'], loc='upper left')
# plt.savefig('acc_gru_256.png')
#
# plt.cla()
# plt.clf()
# plt.close()
# # summarize history for loss
# plt.plot(rms_history.history['loss'])
# plt.plot(rms_history.history['val_loss'])
# plt.title('model loss')
# plt.ylabel('loss')
# plt.xlabel('epoch')
# plt.legend(['train', 'test'], loc='upper left')
# plt.savefig('loss_gru_256.png')

