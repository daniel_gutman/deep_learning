from __future__ import print_function
import keras
from keras.datasets import cifar10
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.optimizers import SGD, Adagrad, Adadelta, RMSprop, Adam
import matplotlib.pyplot as plt
import os

batch_size = 32
num_classes = 10
epochs = 7
data_augmentation = False
num_predictions = 20
save_dir = os.path.join(os.getcwd(), 'saved_models')
model_name = 'keras_cifar10_trained_model.h5'

# The data, split between train and test sets:
(x_train, y_train), (x_test, y_test) = cifar10.load_data()
print('x_train shape:', x_train.shape)
print(x_train.shape[0], 'train samples')
print(x_test.shape[0], 'test samples')

# Convert class vectors to binary class matrices.
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

def cnn_model() : 
    model = Sequential()
    model.add(Conv2D(32, (3, 3), padding='same',
                    input_shape=x_train.shape[1:]))
    model.add(Activation('relu'))
    model.add(Conv2D(32, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Conv2D(64, (3, 3), padding='same'))
    model.add(Activation('relu'))
    model.add(Conv2D(64, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Flatten())
    model.add(Dense(512))
    model.add(Activation('relu'))
    model.add(Dropout(0.5))
    model.add(Dense(num_classes))
    model.add(Activation('softmax'))
    return(model)


def plot_graph(history):
    # Plot training & validation accuracy values
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.title('Model accuracy')
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Test'], loc='upper left')
    plt.show()

    # Plot training & validation loss values
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('Model loss')
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Test'], loc='upper left')
    plt.show()

# initiate RMSprop optimizer
learning_rate = 0.1

rmsprop = keras.optimizers.rmsprop(lr=0.0001, decay=1e-6)
sgd = SGD(lr=learning_rate, momentum=0.0, decay=0.0, nesterov=False) # set to default except lr
sgdm = SGD(lr=learning_rate, momentum=0.9, decay=0.0, nesterov=False) # set to default except lr
adam = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
adagrad = Adagrad(lr=0.01, epsilon=1e-08, decay=0.0)

rms_model = cnn_model()
sgd_model = cnn_model()
sgdm_model = cnn_model()
adam_model = cnn_model()
adagrad_model = cnn_model()
# Let's train the model using RMSprop
rms_model.compile(loss='categorical_crossentropy',
              optimizer=rmsprop,
              metrics=['accuracy'])
sgd_model.compile(loss='categorical_crossentropy',
              optimizer=sgd,
              metrics=['accuracy'])
sgdm_model.compile(loss='categorical_crossentropy',
              optimizer=sgdm,
              metrics=['accuracy'])
adam_model.compile(loss='categorical_crossentropy',
              optimizer=adam,
              metrics=['accuracy'])
adagrad_model.compile(loss='categorical_crossentropy',
              optimizer=adagrad,
              metrics=['accuracy'])

x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255

if not data_augmentation:
    print('Not using data augmentation.')
    history_rms = rms_model.fit(x_train, y_train,
              batch_size=batch_size,
              epochs=epochs,
              validation_data=(x_test, y_test),
              shuffle=True)
    history_sgd = sgd_model.fit(x_train, y_train,
              batch_size=batch_size,
              epochs=epochs,
              validation_data=(x_test, y_test),
              shuffle=True)
    history_sgdm = sgdm_model.fit(x_train, y_train,
              batch_size=batch_size,
              epochs=epochs,
              validation_data=(x_test, y_test),
              shuffle=True)
    history_adam = adam_model.fit(x_train, y_train,
              batch_size=batch_size,
              epochs=epochs,
              validation_data=(x_test, y_test),
              shuffle=True)
    history_adagrad = adagrad_model.fit(x_train, y_train,
              batch_size=batch_size,
              epochs=epochs,
              validation_data=(x_test, y_test),
              shuffle=True)

    # fig = plt.figure(figsize=(12,8))
    plt.plot(range(epochs),history_sgd.history['val_acc'],label='SGD')
    plt.plot(range(epochs),history_sgdm.history['val_acc'],label='SGDM')
    plt.plot(range(epochs),history_adam.history['val_acc'],label='Adam')
    plt.plot(range(epochs),history_rms.history['val_acc'],label='RMSprop')
    plt.plot(range(epochs),history_adagrad.history['val_acc'],label='Adagrad')
    plt.legend(loc=0)
    plt.xlabel('epochs')
    plt.xlim([0,epochs])
    plt.ylabel('accuracy om validation set')
    # plt.grid(True)
    plt.title("Comparing Model Accuracy")
    #plt.show()
    plt.savefig('compare-acc_7.png')
    plt.close()
    plt.cla()
    plt.clf()
    # fig = plt.figure(figsize=(12,8))
    plt.plot(range(epochs),history_sgd.history['val_loss'],label='SGD')
    plt.plot(range(epochs),history_sgdm.history['val_loss'],label='SGDM')
    plt.plot(range(epochs),history_adam.history['val_loss'],label='Adam')
    plt.plot(range(epochs),history_rms.history['val_loss'],label='RMSprop')
    plt.plot(range(epochs),history_adagrad.history['val_loss'],label='Adagrad')
    plt.legend(loc=0)
    plt.xlabel('epochs')
    plt.xlim([0,epochs])
    plt.ylabel('loss om validation set')
    # plt.grid(True)
    plt.title("Comparing Model Loss")
    #plt.show()
    plt.savefig('compare-loss_7.png')
    plt.close()
else:
    print('Using real-time data augmentation.')
    # This will do preprocessing and realtime data augmentation:
    datagen = ImageDataGenerator(
        featurewise_center=False,  # set input mean to 0 over the dataset
        samplewise_center=False,  # set each sample mean to 0
        featurewise_std_normalization=False,  # divide inputs by std of the dataset
        samplewise_std_normalization=False,  # divide each input by its std
        zca_whitening=False,  # apply ZCA whitening
        rotation_range=0,  # randomly rotate images in the range (degrees, 0 to 180)
        width_shift_range=0.1,  # randomly shift images horizontally (fraction of total width)
        height_shift_range=0.1,  # randomly shift images vertically (fraction of total height)
        horizontal_flip=True,  # randomly flip images
        vertical_flip=False)  # randomly flip images

    # Compute quantities required for feature-wise normalization
    # (std, mean, and principal components if ZCA whitening is applied).
    datagen.fit(x_train)

    # Fit the model on the batches generated by datagen.flow().
    rms_model.fit_generator(datagen.flow(x_train, y_train,
                                     batch_size=batch_size),
                        epochs=epochs,
                        validation_data=(x_test, y_test),
                        workers=4)

    sgd_model.fit_generator(datagen.flow(x_train, y_train,
                                     batch_size=batch_size),
                        epochs=epochs,
                        validation_data=(x_test, y_test),
                        workers=4)

    sgdm_model.fit_generator(datagen.flow(x_train, y_train,
                                     batch_size=batch_size),
                        epochs=epochs,
                        validation_data=(x_test, y_test),
                        workers=4)

    adam_model.fit_generator(datagen.flow(x_train, y_train,
                                     batch_size=batch_size),
                        epochs=epochs,
                        validation_data=(x_test, y_test),
                        workers=4)

    adagrad_model.fit_generator(datagen.flow(x_train, y_train,
                                     batch_size=batch_size),
                        epochs=epochs,
                        validation_data=(x_test, y_test),
                        workers=4)

# Save model and weights
# if not os.path.isdir(save_dir):
#     os.makedirs(save_dir)
# model_path = os.path.join(save_dir, model_name)
# model.save(model_path)
# print('Saved trained model at %s ' % model_path)

# Score trained model.
rms_scores = rms_model.evaluate(x_test, y_test, verbose=0)
sgd_scores = sgd_model.evaluate(x_test, y_test, verbose=0)
sgdm_scores = sgdm_model.evaluate(x_test, y_test, verbose=0)
adam_scores = adam_model.evaluate(x_test, y_test, verbose=0)
adagrad_scores = adagrad_model.evaluate(x_test, y_test, verbose=0)
print("RMSProps Accuracy: %.2f%%" % (rms_scores[1]*100))
print("RMSProps Loss: %.2f%%" % (rms_scores[0]*100))
print("SGD Accuracy: %.2f%%" % (sgd_scores[1]*100))
print("SGD Loss: %.2f%%" % (sgd_scores[0]*100))
print("SGDM Accuracy: %.2f%%" % (sgdm_scores[1]*100))
print("SGDM Loss: %.2f%%" % (sgdm_scores[0]*100))
print("ADAM Accuracy: %.2f%%" % (adam_scores[1]*100))
print("ADAM Loss: %.2f%%" % (adam_scores[0]*100))
print("ADAGRAD Accuracy: %.2f%%" % (adagrad_scores[1]*100))
print("ADAGRAD Loss: %.2f%%" % (adagrad_scores[0]*100)) 
